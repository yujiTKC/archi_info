//展覧会のビュー
	
function lecwin1(title) {
	//var url = 'http://rss.cnn.com/services/podcasting/newscast/rss.xml';
	var url = 'https://spreadsheets.google.com/feeds/list/0AueUKt-0rlfUdHBwQks1T2pKUXQwQk54UDhicHZkeWc/od6/public/basic?alt=rss';

	var data;
	var i = 0;
	var feedTableView;
	var feedTitle = '';
	
	var self = Ti.UI.createWindow({
		title:title,
		barColor:'#87CEFA',
		exitOnClose: true
	});
	
	// var stream = Ti.Media.createAudioPlayer();
	
	var item_window = Ti.UI.createView({
		backgroundColor:'#87CEFA',
		height:'25dp',
		bottom:'50dp'
	});
	self.add(item_window);
	
	var item_title_label = Ti.UI.createLabel({
		text: '',
		color: '#fff',
		textAlign:'center',
		left:'10dp',
		right:'10dp',
		top:'5dp',
		height:'auto',
		font:{fontFamily:'Helvetica Neue',fontWeight:'bold',fontSize:18}
		});
	item_window.add(item_title_label);
	
	function displayItems(itemList){
	
		for (var c=0;c < itemList.length;c++){	
			
			var title = null;
			var desc = null;
			var startDate = null;
			var mydate = null;
			var brief = null;
			var address = null;
			var speaker = null;
			var exurl = null;
			var mapurl = null;
			var region = null;
			var lecDataArray = [];
			var localImage = null;
	
			desc = itemList.item(c).getElementsByTagName("description").item(0).text;
				
			var descArray = desc.split(", ");
			Ti.API.info(desc);
		    title     = descArray[0].replace("ex-name:","");
			startDate = descArray[1].replace("start-date:","");
			// endDate   = descArray[2].replace("end-date:","");
			mydate    = startDate;
			brief     = descArray[2].replace("brief:","");
			address   = descArray[3].replace("address:","");
			speaker   = descArray[4].replace("speaker:","");
			exurl     = descArray[5].replace("ex-url: ","");
			mapurl    = descArray[6].replace("map-url: ","");
			region    = descArray[7].replace("tagregion: ","");
			
			lecDataArray = [title,mydate,address,speaker,exurl,brief,mapurl]; 
			Ti.API.info(descArray);
			
			switch(region){
				case "HOKKAIDO":
				case "TOHOKU":
				localImage = "local_icon-01.png";
				break;
				case "KANTO":
				localImage = "local_icon-02.png";
				break;
				case "CHUBU":
				localImage = "local_icon-03.png";
				break;
				case "KANSAI":
				localImage = "local_icon-04.png";
				break;
				case "CHUGOKU":
				case "SHIKOKU":
				localImage = "local_icon-05.png";
				break;
				case "KYUSYU":
				localImage = "local_icon-06.png";
				break;
				default:
				localImage = "local_icon-07.png";
				break;
				
			}


				
			var lecRow = Ti.UI.createTableViewRow({
				height:'50dp',
				backgroundColor:'#ffffff',
				backgroundSelectedColor:'#3DB5E4'
				// layout:'vertical'
			}); 
			
				// Create a label for the title
			var post_title = Ti.UI.createLabel({
				text: title,
				color: '#000',
				textAlign:'left',
				left:'80dp',
				height:'20dp',
				width:'auto',
				top:'7dp',
				font:{fontWeight:'bold',fontSize:'16dp'},
				wordWrap : false
			});
				
			var sub_title = Ti.UI.createLabel({
				text: mydate,
				color: '#000',
				textAlign:'left',
				left:'80dp',
				height:'auto',
				width:'auto',
				bottom:'7dp',
				font:{fontsize:'8dp'},
				wordWrap : false
			});
				
			lecRow.add(post_title);
			lecRow.add(sub_title);
				
				// add the CNN logo on the left
				// naturally this could be an image in the feed itself if it existed
			var item_image = Ti.UI.createImageView({
				image:localImage,
				left:'0dp',
				top:'5dp',
				width:'80dp',
				height:'40dp'
			});
			
			lecRow.add(item_image);
			lecRow.hasChild = true;	
			// Add some rowData for when it is clicked			
			// row.thisTitle = title;
			// row.thisDesc = desc;
			lecRow.datas = lecDataArray;
			lecRow.mapurl = mapurl;
				
			// Add the row to the data
			data[i] = lecRow;
			i++;
				
		}  //for (var c=0;c < itemList.length;c++){	
		
		// create the table
		if(Ti.Platform.osname !='android'){
			feedTableView = Titanium.UI.createTableView({
				data:data,
				top:0,
				width:'100%',
				bottom:75,
				separatorColor: '#87CEFA' //SkyBlue
			});
		}else{
			feedTableView = Titanium.UI.createTableView({
				data:data,
				top:0,
				width:'100%',
				bottom:75,
				separatorColor: '#000000' //Black
			});
		}
		
		// Add the tableView to the current window
		self.add(feedTableView);
		
		// Create tableView row event listener
		
		feedTableView.addEventListener('click', function(e){
			
			// a feed item was clicked
			Ti.API.info('item index clicked :'+e.index);
			// Ti.API.info(e.rowData.title);
			
			var funcwin2 = require('lecwin2');
			// dataArray:e.rowData.datas,mapurl:e.rowData.mapurl,siteurl:e.rowData.siteurl,
			Ti.API.info(e.row.datas);
			Ti.API.info(e.row.datas[3]);
			var win2 = new funcwin2(e.row.datas,{containingTab:self.containingTab,tabGroup:self.tabGroup});

			self.containingTab.open(win2,{animated:true});
		});
		
	} //displayItems end
	
	function loadRSSFeed(url){
	
		data = [];
		Ti.API.info('>>>> loading RSS feed '+url);
		xhr = Titanium.Network.createHTTPClient();
		xhr.open('GET',url);
		xhr.onload = function()
		{
				
			Ti.API.info('>>> got the feed! ... ');
			
			// Now parse the feed XML 
			var xml = this.responseXML;
			
			// Find the channel element 
			var channel = xml.documentElement.getElementsByTagName("channel");
	
			// feedTitle = channel.item(0).getElementsByTagName("title").item(0).text;
			
			Ti.API.info("FEED TITLE " + feedTitle);
			
			self.title = '展覧会';
			// Find the RSS feed 'items'
			var itemList = xml.documentElement.getElementsByTagName("item");
			Ti.API.info('found '+itemList.length+' items in the RSS feed');
	
			item_title_label.text = 'DONE';
			// item_desc_label.text = 'click a feed item';
			
			// Now add the items to a tableView
			displayItems(itemList);
	
		};
		
		item_title_label.text = 'LOADING RSS FEED..';
		// item_desc_label.text = '';
		xhr.send();	
	}

	// RIGHT NAVBAR REFRESH BUTTON		
	var r = Titanium.UI.createButton({
		systemButton:Titanium.UI.iPhone.SystemButton.REFRESH
	});
	r.addEventListener('click',function()
	{
		// reload feed
		loadRSSFeed(url);	
	});

	
	self.addEventListener('open', function() {
		// load the feed
		loadRSSFeed(url);
	});
	
	Ti.include('createAdView.js');
	//if(Ti.Platform.osname !='android'){
	self.add(createAdView());
	//}

		
	return self;
};

module.exports = lecwin1;
