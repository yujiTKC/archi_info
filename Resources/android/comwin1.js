//展覧会のビュー
	
function comwin1(title) {
	//var url = 'http://rss.cnn.com/services/podcasting/newscast/rss.xml';
	var url = 'https://spreadsheets.google.com/feeds/list/0AueUKt-0rlfUdDlBZUVtWHlFam5yWi0xd2dqeG56dUE/od6/public/basic?alt=rss';

	var data;
	var i = 0;
	var feedTableView;
	var feedTitle = '';
	
	var self = Ti.UI.createWindow({
		title:title,
		barColor:'#87CEFA',
		exitOnClose: true
	});
	
	// var stream = Ti.Media.createAudioPlayer();
	
	var item_window = Ti.UI.createView({
		backgroundColor:'#87CEFA',
		height:'25dp',
		bottom:'50dp'
	});
	self.add(item_window);
	
	var item_title_label = Ti.UI.createLabel({
		text: '',
		color: '#fff',
		textAlign:'center',
		left:'10dp',
		right:'10dp',
		top:'5dp',
		height:'auto',
		font:{fontFamily:'Helvetica Neue',fontWeight:'bold',fontSize:18}
		});
	item_window.add(item_title_label);
	
	function displayItems(itemList){
	
		for (var c=0;c < itemList.length;c++){	
			
			var title = null;
			var desc = null;
			var startDate = null;
			var endDate = null;
			var brief = null;
			var address = null;
			var judge = null;
			var exurl = null;
			var prize = null;
	
			desc = itemList.item(c).getElementsByTagName("description").item(0).text;
				
			var descArray = desc.split(", ");
			Ti.API.info(desc);
		    title     = descArray[0].replace("ex-name:","");
			startDate = "登録締め切り："+descArray[1].replace("start-date:","");
			endDate   = "提出締め切り："+descArray[2].replace("end-date:","");

			brief     = descArray[3].replace("brief:","");
			prize     = descArray[4].replace("prize: ","");
			judge     = descArray[5].replace("judge: ","");
			exurl     = descArray[6].replace("ex-url: ","");
			
			dataArray = [title,startDate,endDate,prize,judge,exurl,brief]; 
			Ti.API.info("region is ");
			Ti.API.info(descArray);
			
				
			var row = Ti.UI.createTableViewRow({
				height:'70dp',
				backgroundColor:'#ffffff',
				backgroundSelectedColor:'#3DB5E4'
				// layout:'vertical'
			}); 
			
				// Create a label for the title
			var post_title = Ti.UI.createLabel({
				text: title,
				color: '#000',
				textAlign:'left',
				left:'20dp',
				height:'20dp',
				width:'auto',
				top:'7dp',
				font:{fontWeight:'bold',fontSize:'16dp'},
				wordWrap : false
			});
				
			var sub_title1 = Ti.UI.createLabel({
				text: startDate,
				color: '#000',
				textAlign:'left',
				left:'20dp',
				height:'auto',
				width:'auto',
				bottom:'25dp',
				font:{fontsize:'7dp'}
			});
			var sub_title2 = Ti.UI.createLabel({
				text: endDate,
				color: '#000',
				textAlign: 'left',
				left: '20dp',
				height:'auto',
				width:'auto',
				bottom:'5dp',
				font:{fontsize:'7dp'}
			});
				
			row.add(post_title);
			row.add(sub_title1);
			row.add(sub_title2);
				
			row.hasChild = true;	

			row.datas = dataArray;
				
			// Add the row to the data
			data[i] = row;
			i++;
				
		}  //for (var c=0;c < itemList.length;c++){	
		
		// create the table
		if(Ti.Platform.osname !='android'){
			feedTableView = Titanium.UI.createTableView({
				data:data,
				top:0,
				width:'100%',
				bottom:75,
				separatorColor: '#87CEFA' //SkyBlue
			});
		}else{
			feedTableView = Titanium.UI.createTableView({
				data:data,
				top:0,
				width:'100%',
				bottom:75,
				separatorColor: '#000000' //Black
			});
		}
		
		// Add the tableView to the current window
		self.add(feedTableView);
		
		// Create tableView row event listener
		
		feedTableView.addEventListener('click', function(e){
			
			// a feed item was clicked
			Ti.API.info('item index clicked :'+e.index);
			// Ti.API.info(e.rowData.title);
			
			var funcwin2 = require('comwin2');
			// dataArray:e.rowData.datas,mapurl:e.rowData.mapurl,siteurl:e.rowData.siteurl,
			Ti.API.info(e.row.datas);
			var win2 = new funcwin2(e.row.datas,{containingTab:self.containingTab,tabGroup:self.tabGroup});

			self.containingTab.open(win2,{animated:true});
		});
		
	} //displayItems end
	
	function loadRSSFeed(url){
	
		data = [];
		Ti.API.info('>>>> loading RSS feed '+url);
		xhr = Titanium.Network.createHTTPClient();
		xhr.open('GET',url);
		xhr.onload = function()
		{
				
			Ti.API.info('>>> got the feed! ... ');
			
			// Now parse the feed XML 
			var xml = this.responseXML;
			
			// Find the channel element 
			var channel = xml.documentElement.getElementsByTagName("channel");
	
			// feedTitle = channel.item(0).getElementsByTagName("title").item(0).text;
			
			Ti.API.info("FEED TITLE " + feedTitle);
			
			self.title = '展覧会';
			// Find the RSS feed 'items'
			var itemList = xml.documentElement.getElementsByTagName("item");
			Ti.API.info('found '+itemList.length+' items in the RSS feed');
	
			item_title_label.text = 'DONE';
			// item_desc_label.text = 'click a feed item';
			
			// Now add the items to a tableView
			displayItems(itemList);
	
		};
		
		item_title_label.text = 'LOADING RSS FEED..';
		// item_desc_label.text = '';
		xhr.send();	
	}

	// RIGHT NAVBAR REFRESH BUTTON		
	var r = Titanium.UI.createButton({
		systemButton:Titanium.UI.iPhone.SystemButton.REFRESH
	});
	r.addEventListener('click',function()
	{
		// reload feed
		loadRSSFeed(url);	
	});

	
	self.addEventListener('open', function() {
		// load the feed
		loadRSSFeed(url);
	});
	
	Ti.include('createAdView.js');
	//if(Ti.Platform.osname !='android'){
	self.add(createAdView());
	//}

		
	return self;
};

module.exports = comwin1;
