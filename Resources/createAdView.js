/**
    広告描画用のメソッド
 
    モジュールをプロジェクトに追加している必要がある。
    各種識別コードに関しては（config/ads.js）に記載している
*/
 
Titanium.Nend  = require('net.nend');

function createAdView(){
 	Ti.API.info('create AdView');
    var rand = Math.floor(Math.random()*2);
    if(Ti.Platform.osname =='android'){
    	var Admob = require('ti.admob');
    	var adview = Admob.createView({
    		publisherId:"a1522975b99908a",
    		testing:false, // default is false
    		//top: 10, //optional
    		//left: 0, // optional
    		//right: 0, // optional
		    bottom: 0, // optional
    		adBackgroundColor:"FF8855", // optional
    		backgroundColorTop: "738000", //optional - Gradient background color at top
    		borderColor: "#000000", // optional - Border color
    		textColor: "#000000", // optional - Text color
    		urlColor: "#00FF00", // optional - URL color
    		linkColor: "#0000FF" //optional -  Link text color
    		//primaryTextColor: "blue", // deprecated -- now maps to textColor
    		//secondaryTextColor: "green" // deprecated -- now maps to linkColor
    	});
    

    	
           	/*
           	        	if(Ti.Platform.osname == 'android'){
            	var adview = Titanium.Nend.createView({
                	spotId: 86117,
                	apiKey: '52318c72369b337b998d3719b702c8e5d5695765',
                	width:  320,
                	height: 50,
                	bottom: 0,
                	left:   0
            	});
           }
           	
           	*/
    }
    else{
    switch(rand){ 
        case 0: //nend
			var adview = Titanium.Nend.createView({
                spotId: 82588,
                apiKey: 'd6b2161abb8f5b787e59d95eb81e0cd9cc49a5f9',
                width:  320,
                height: 50,
                bottom: 0,
                left:   0,
                adBackgroundColor:'blue'
                });
            break;
        case 1: //appbank
           	var adview = Titanium.Nend.createView({
               	spotId: 86123,
                apiKey: '256539c747fe415b0ef08001b339827627495637',
               	width:  320,
               	height: 50,
               	bottom: 0,
               	left:   0
           		});
            break;
 
        default:
            var adview = null;
            break;
 		}
    }
 
    return adview;
 
}