function sitewin(_args) {
	
	//var funcwin2 = require('exwin2');
	//var win2 = new funcwin2({dataArray:e.rowData.datas,containingTab:self.containingTab,tabGroup:self.tabGroup});

	
	Ti.API.info(_args);

	var self = Ti.UI.createWindow({
		title:'',
		barColor:'#b40000'
	});
	
	var mywidth = Ti.Platform.displayCaps.platformWidth;
	var myheight = Ti.Platform.displayCaps.platformHeight;
	
	Ti.API.info('mywidth & height');
	Ti.API.info(mywidth+'  '+myheight);

	var myurl = _args.url.replace(" ","");
	var webview = Ti.UI.createWebView({
		url:myurl,
		bottom:'50dp'
		});
	Ti.API.info("weburl ="+webview.url);

	webview.addEventListener('beforeload',function(e){
    	Ti.API.info("読み込み開始: "+e.url);
	});
	webview.addEventListener('load',function(e){
    	Ti.API.info("読み込み完了: "+e.url);
	});
	webview.addEventListener('error', function(e){
    	Ti.API.info("読み込み失敗: "+e.url);
	});


	self.add(webview);
	Ti.include('createAdView.js');
	// if(Ti.Platform.osname !='android'){
	self.add(createAdView());
	// }
	return self;
}
module.exports = sitewin;