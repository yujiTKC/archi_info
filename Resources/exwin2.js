function exwin2(dataArray,_args) {
	Ti.include('createAdView.js');
	//var funcwin2 = require('exwin2');
	//var win2 = new funcwin2({dataArray:e.rowData.datas,containingTab:self.containingTab,tabGroup:self.tabGroup});
// dataArray = [title,mydate,address,exurl,brief,mapurl];
	var mywidth = Ti.Platform.displayCaps.platformWidth;
	var myheight = Ti.Platform.displayCaps.platformHeight;
	
	Ti.API.info('mywidth & height');
	Ti.API.info(mywidth+'  '+myheight);
	Ti.API.info(Ti.UI.SIZE);
	
	var self = Ti.UI.createWindow({
		title:"",
		barColor:'#b40000',
		height:'100%'
	});
	
	Ti.API.info(self.getHeight());
	
	var data = [];
	var i = 0;
	var TableView;
	
	// var dataArray = _args.dataArray;
	var dataFontArray = ['24dp','20dp','20dp','20dp','12dp'];
	var mapURL = dataArray[5];
	var siteURL = dataArray[3];
	
	Ti.API.info('dataArray is ');
	Ti.API.info(dataArray);
	var l = dataArray.length-1;
	
	for (var c=0;c < l ;c++){	
		var row = Ti.UI.createTableViewRow({
			height:'auto',
			backgroundColor:'#ffffff',
			// selectedBackgroundColor:'#b40000',
			backgroundSelectedColor:'#b40000',
				// layout:'vertical'
		}); 
		
		var row_title = Ti.UI.createLabel({
			color:'#000000',
			text:dataArray[c],
			font:{fontSize:dataFontArray[c]},
			textAlign:'left',
			top:'10dp',
			bottom:'10dp'
			// left:20
		});
		
		switch(c){
			case 0:
			case 1:
			case 4:
			break;
			case 2:
				row.hasChild = true;
			break;
			case 3:
				row.hasChild = true;
			break;
			default:
			break;
			
		}
		
		row.add(row_title);		
		data[c] = row;
	}
	
	TableView = Titanium.UI.createTableView({
			data:data,
			top:0,
			bottom:'50dp',
			width:'100%',
			height:'auto',
			separatorColor: '#000000' //SkyBlue
	});

	TableView.addEventListener('click', function(e){
    if (e.rowData.hasChild) {
    	Ti.API.info('index is' + e.index);
    	switch(e.index){
    		case 2:
    		var funcsitewin = require('sitewin');
    		var siteWin = new funcsitewin({url:mapURL,containingTab:_args.containingTab,tabGroup:_args.tabGroup});
    		_args.containingTab.open(siteWin,{animated:true});
    		break;
    		case 3:
    		var funcsitewin = require('sitewin');
    		var siteWin = new funcsitewin({url:siteURL,containingTab:_args.containingTab,tabGroup:_args.tabGroup});
    		_args.containingTab.open(siteWin,{modal:true});
    		break;
    	}
 
    	}
	});	
	
	
	
	self.add(TableView);
	// if(Ti.Platform.osname !='android'){
	self.add(createAdView());
	// }

	return self;
};
module.exports = exwin2;
