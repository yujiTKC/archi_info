
var tabGroup = Ti.UI.createTabGroup();
// tabGroup.tabsBackgroundColor = '#000';
tabGroup.tabsBackgroundImage = 'background.png';
tabGroup.activeTabBackgroundImage = 'select.png';
    // e.shadowImage = 'images/shadowimage.png';
// .tabsBackgroundImage = 'images/tabbar.png';
// tabGroup.activeTabBackgroundImage = 'images/activetab.png';
tabGroup.activeTabIconTint = '#87CEFA';

/* 
var win1 = Ti.UI.createWindow({
	url:'win1.js',
	backgroundColor:'green'
});
*/
if(Ti.Platform.osname =='android'){
	var funcwin1 = require('android/exwin1');
	var win1 = new funcwin1(L('展覧会一覧'));

	var funcwin2 = require('android/lecwin1');
	var win2 = new funcwin2(L('講演会一覧'));

	var funcwin3 = require('android/comwin1');
	var win3 = new funcwin3(L('コンペ一覧'));
}else{
	var funcwin1 = require('iphone/exwin1');
	var win1 = new funcwin1(L('展覧会一覧'));

	var funcwin2 = require('iphone/lecwin1');
	var win2 = new funcwin2(L('講演会一覧'));

	var funcwin3 = require('iphone/comwin1');
	var win3 = new funcwin3(L('コンペ一覧'));	
}

/*
var win4 = Ti.UI.createWindow({
	title: 'お気に入り',
	backgroundColor: 'black'
});
*/
//------------------------------------------
//	タブの設定
//------------------------------------------

if (Titanium.Platform.name == 'android'){
	var tab1 = Ti.UI.createTab({
    	icon: 'exhibition.png',
    	title: '展覧会',
    	window: win1
	});	
	
	var tab2 = Ti.UI.createTab({
    	icon: 'lecture.png',
    	title: '講演会',
	    window: win2
	});
	
	var tab3 = Ti.UI.createTab({
    	icon: 'compe.png',
    	title: 'コンペ',
    	window: win3
	});
/*
	var tab4 = Ti.UI.createTab({
    	icon: 'favorite3b.png',
	    title: 'お気に入り',
    	window: win4
	}); 
	*/
} else{
	var tab1 = Ti.UI.createTab({
    	icon: 'exhibition.png',
    	title: '展覧会',
    	window: win1
	});
	
	var tab2 = Ti.UI.createTab({
    	icon: 'lecture.png',
    	title: '講演会',
	    window: win2
	});
	
	var tab3 = Ti.UI.createTab({
    	icon: 'compe.png',
    	title: 'コンペ',
    	window: win3
	});
/*
	var tab4 = Ti.UI.createTab({
    	icon: 'favorite.png',
	    title: 'later',
    	window: win4
	}); 
*/ 
}



win1.containingTab = tab1;
win2.containingTab = tab2;
win3.containingTab = tab3;
// win4.containingTab = tab4;


tabGroup.addTab(tab1);
tabGroup.addTab(tab2);
tabGroup.addTab(tab3);
// tabGroup.addTab(tab4);
tabGroup.open();


if (Titanium.Platform.name == 'iPhone OS')
{ 
	tabGroup.tabs[0].setBadge(2);
}
Ti.API.info(tabGroup.tabs[0].title);




